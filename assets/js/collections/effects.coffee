Backbone = require 'backbone'
App      = require '../application.coffee'
Effect   = require '../models/effect.coffee'

class Effects extends Backbone.Collection
  model: Effect

module.exports = Effects
