Marionette        = require 'backbone.marionette'
App               = require './application'
EffectsCollection = require './collections/effects'
DAW               = require './daw'
UserModel         = require './models/user'
RegisterView      = require './views/register'
SingleView        = require './views/single'
PreviewTrackView  = require './views/preview'
EffectsView       = require './views/effects'
RecordView        = require './views/record'
FinishView        = require './views/finish'

prepared_effects  = require './effects/index'

class Controller
  root: ->
    initialize_daw()
    user = initialize_user()
    App.main.show(new RegisterView(model: user))

  rules: ->
    App.main.show(new SingleView(type: 'rules'))

  terms: ->
    App.main.show(new SingleView(type: 'terms'))

  preview_track: ->
    initialize_daw()
    App.main.show(new PreviewTrackView)

  select_effect: ->
    initialize_daw()
    effects = new EffectsCollection(prepared_effects)
    App.main.show(new EffectsView(collection: effects))

  record: ->
    initialize_daw()
    App.main.show(new RecordView)

  finish: ->
    App.main.show(new FinishView)

  initialize_user = ->
    user = new UserModel
    App.reqres.setHandler('user', -> user)
    user

  initialize_daw = ->
    if App.request('daw') then return
    daw = new DAW
    App.reqres.setHandler('daw', -> daw)
    if not daw.voice then daw.setup_voice()
    daw

module.exports = Controller
