$   = require 'jquery'
Wad = require 'wad'
Tuna = require 'tuna'

Wad::constructExternalFx = (arg, ctx) ->
  @tuna = new Tuna(ctx)
  @chorus = arg.chorus
  @overdrive = arg.overdrive

Wad::setUpExternalFxOnPlay = (arg, context) ->
  if arg.chorus
    chorus = new @tuna.Chorus
      rate: arg.chorus.rate or @chorus.rate
      feedback: arg.chorus.feedback or @chorus.feedback
      delay: arg.chorus.delay or @chorus.delay
      bypass: arg.chorus.bypass or @chorus.bypass

    chorus.input.connect = chorus.connect.bind(chorus)
    @nodes.push(chorus.input)

  if arg.overdrive
    overdrive = new @tuna.Overdrive
      outputGain: arg.overdrive.outputGain or @overdrive.outputGain
      drive: arg.overdrive.drive or @overdrive.drive
      curveAmount: arg.overdrive.curveAmount or @overdrive.curveAmount
      algorithmIndex: arg.overdrive.algorithmIndex or @overdrive.algorithmIndex
      bypass: arg.overdrive.bypass or @overdrive.bypass

    overdrive.input.connect = overdrive.connect.bind(overdrive)
    @nodes.push(overdrive.input)

###*
 * A digital audio workstation, for playing and recording audio.
###

class DAW

  constructor: ->
    @mixer = new Wad.Poly
      recConfig:
        workerPath: '/js/vendor/recorder_worker.js'
      compressor:
        attack: .003
        knee: 30
        ratio: 12
        release: .25
        threshold: -24

    @background_track = new Wad
      source: '/audio/background.mp3'
      volume: .5
      env:
        hold: 2000

    @sample_track = new Wad
      source: '/audio/sample.mp3'
      volume: .7
      env:
        hold: 2000

  setup_voice: ->
    @voice = new Wad
      source: 'mic'
      chorus:
        rate: 1.5
        feedback: 0.2
        delay: 0.0045
        bypass: 0

  record: ->
    @mixer.add(@background_track)
    @mixer.add(@voice)

    if @mixer.rec then @mixer.rec.clear()

    @mixer.rec.record()
    @background_track.play()
    @voice.play()

  stop: ->
    @background_track.stop()
    @voice.stop()
    @mixer.rec.stop()
    @mixer.rec.createWad()

  play: ->
    @mixer.rec.recordings[0].play()

  stop_play: ->
    @mixer.rec.recordings[0].stop()

module.exports = DAW
