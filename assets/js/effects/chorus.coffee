Effect = require '../models/effect'

module.exports = new Effect
  name: 'Chorus'
  params:
    chorus:
      rate: 1.5
      feedback: 0.5
      delay: 0.0045
      bypass: 0
    reverb:
      wet: 0.05
      impulse: '/audio/impulse.wav'
