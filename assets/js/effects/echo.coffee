Effect = require '../models/effect'

module.exports = new Effect
  name: 'Echo'
  params:
    delay:
      delayTime: .25
      wet: .2
      feedback: .3
    reverb:
      wet: 0.02
      impulse: '/audio/impulse.wav'
