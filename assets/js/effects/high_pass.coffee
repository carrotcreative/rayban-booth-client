Effect = require '../models/effect'

module.exports = new Effect
  name: 'High Pass'
  params:
    filter:
      type: 'highpass'
      frequency : 700
