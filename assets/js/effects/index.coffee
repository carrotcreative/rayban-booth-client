module.exports = [
  require('./reverb.coffee')
  require('./echo.coffee')
  require('./chorus.coffee')
  require('./overdrive.coffee')
  require('./nothing.coffee')
]
