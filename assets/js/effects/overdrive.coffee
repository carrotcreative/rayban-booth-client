Effect = require '../models/effect'

module.exports = new Effect
  name: 'Overdrive'
  params:
    overdrive:
      outputGain: 0
      drive: 0.6
      curveAmount: 0.7
      algorithmIndex: 0
      bypass: 0
