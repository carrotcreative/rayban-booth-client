Effect = require '../models/effect'

module.exports = new Effect
  name: 'Reverb'
  params:
    reverb:
      wet: 0.25
      impulse: '/audio/impulse.wav'
