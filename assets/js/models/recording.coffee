Backbone = require 'backbone'
$        = require 'jquery'
config   = require '../config.coffee'
App      = require '../application.coffee'

class Recording extends Backbone.Model

  save: (args) ->
    mixer = @get('audio').mixer

    mixer.rec.exportWAV (blob) ->
      fd = new FormData
      fd.append("recording", blob)
      fd.append("user_id", App.request('user').get('id'))

      $.ajax
        type: 'POST'
        url: "#{config.api_url}/upload"
        data: fd
        cache: false
        contentType: false
        processData: false
        success: args.success
        error: args.error

module.exports = Recording
