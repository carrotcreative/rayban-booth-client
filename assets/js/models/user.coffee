Backbone = require 'backbone'
config   = require '../config.coffee'

class User extends Backbone.Model
  url: "#{config.api_url}/users"

module.exports = User
