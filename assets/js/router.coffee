Marionette = require 'backbone.marionette'
App        = require './application.coffee'

class Router extends Marionette.AppRouter
  appRoutes:
    "": "root"
    "rules": "rules"
    "terms": "terms"
    "preview": "preview_track"
    "effects": "select_effect"
    "record": "record"
    "finish": "finish"

module.exports = Router
