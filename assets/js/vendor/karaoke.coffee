module.exports = (opts) ->
  el = opts.el
  keys = Object.keys(opts.lyrics)

  el.innerHTML = opts.lyrics[keys[0]]

  time = 0.0
  timer = setInterval (t) ->
    time_key = time.toFixed(1)
    match = keys.indexOf(time_key)
    if match > -1
      time_to_next = keys[match+1] - keys[match]
      if isNaN(time_to_next) then time_to_next = 30 - keys[match]
      time_to_next -= .5
      if opts.lyrics[keys[match]] isnt 'pause'
        el.innerHTML = "#{opts.lyrics[keys[match]]}<span class='line'></span>"
        line = el.children[0]
        line.style.webkitAnimationDuration = "#{Math.round(time_to_next*10)/10}s"

    time += 0.1
  , 100
