Marionette = require 'backbone.marionette'
$          = require 'jquery'
App        = require '../application'

class Effect extends Marionette.ItemView
  template: templates.effect
  tagName: 'li'

  events:
    'click': 'click'

  click: (e) ->
    @trigger('effect:select')

module.exports = Effect
