Marionette = require 'backbone.marionette'
App        = require '../application'
Effect     = require './effect.coffee'

class Effects extends Marionette.CompositeView
  template: templates.effects
  childView: Effect
  childViewContainer: 'ul'
  id: 'effects'

  events:
    'click .next': 'next'

  childEvents:
    'effect:select': 'select'

  initialize: ->
    @daw = App.request('daw')

  onRender: ->
    @$el.find('ul li:last-child').click()

  select: (e) ->
    e.$el.siblings().removeClass('active')
    e.$el.addClass('active')
    @daw.voice.stop()

    if e.$el.hasClass('active')
      @daw.voice.play(e.model.get('params'))
    else
      @daw.voice.stop()

  next: ->
    if @daw.voice.mediaStreamSource then @daw.voice.stop()
    if @daw.background_track.playing then stop_track.call(@)
    App.request('router').navigate('record', trigger: true)

module.exports = Effects
