Marionette = require 'backbone.marionette'
App        = require '../application.coffee'

class Finish extends Marionette.ItemView
  template: templates.finish
  id: 'finish'

  events:
    'click .restart': 'restart'

  onRender: ->
    setTimeout((=> @restart()), 10000)

  restart: ->
    App.request('router').navigate('', trigger: true)

module.exports = Finish
