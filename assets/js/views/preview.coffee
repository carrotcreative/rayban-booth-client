Marionette = require 'backbone.marionette'
App        = require '../application.coffee'

class Preview extends Marionette.ItemView
  template: templates.preview_track
  id: 'preview'

  ui:
    'track': '.track'

  events:
    'click .next': 'next'
    'click .track': 'preview_track'

  initialize: ->
    @daw = App.request('daw')

  preview_track: ->
    track = @daw.sample_track

    if track.playing
      stop_track.call(@)
    else
      play_track.call(@)
      @track_timer = setTimeout =>
        stop_track.call(@)
      , 30000

  next: ->
    if @daw.sample_track.playing then @daw.sample_track.stop()
    App.request('router').navigate('effects', trigger: true)

  # private
  
  play_track = (track) ->
    track = @daw.sample_track
    track.play()
    track.playing = true
    @ui.track.addClass('active')

  stop_track = (track) ->
    clearTimeout(@track_timer)
    track = @daw.sample_track
    track.stop()
    track.playing = false
    @ui.track.removeClass('active')

module.exports = Preview
