Marionette = require 'backbone.marionette'
App        = require '../application.coffee'
Recording  = require '../models/recording'
karaoke    = require '../vendor/karaoke.coffee'

$ = require 'jquery'

class Record extends Marionette.ItemView
  template: templates.record
  id: 'record'

  ui:
    recording: '.recording'
    record_button: '.recording .button'
    record_instructions: '.recording .instructions'
    record_preroll: '.recording .preroll'
    countdown: '.countdown'
    lyrics: '.lyrics'
    actions: '.actions'
    playback: '.playback'
    re_record: '.re-record'
    next: '.next'
    bars: '.bars'

  events:
    'click @ui.record_button': 'record'
    'click @ui.playback': 'playback'
    'click @ui.re_record': 'record'
    'click @ui.next': 'next'

  initialize: ->
    @daw = App.request('daw')

  record: ->
    # stop playback if any is happening
    if @daw.playing then @daw.stop_play()

    # show recording section
    @ui.recording.show()
    @ui.record_instructions.text('Get ready to sing...')

    # hide followup actions and next button if present
    @ui.actions.hide()

    # preroll countdown
    @ui.record_button.hide()
    @ui.record_preroll.show()
    @countdown @ui.record_preroll, 3, =>

      # hide the 'start recording' ui
      @ui.record_preroll.hide()
      @ui.recording.hide()
      @ui.record_button.show()

      # show the lyrics and countdown
      @ui.lyrics.show()
      @display_lyrics()
      @ui.countdown.show()
      @countdown(@ui.countdown, 30)

      # start recording
      @daw.record()

      # start the music visualizer
      @ui.bars.show()
      @visualize(@daw.mixer.input)

      # after 20 seconds
      setTimeout =>

        # stop recording
        @daw.stop()

        # hide the lyrics, countdown, and visualizer
        @ui.lyrics.hide()
        @ui.countdown.hide()
        setTimeout((=> @ui.bars.hide()), 1000)

        # show the followup actions and next button
        @ui.actions.show()
        
      , 30000

  visualize: (analyser) ->
    canvas = @ui.bars[0]
    ctx = canvas.getContext('2d')
    width = canvas.width
    height = canvas.height

    analyser.fftSize = 256
    bufferLength = analyser.frequencyBinCount
    dataArray = new Uint8Array(bufferLength)
    ctx.clearRect(0, 0, width, height)

    draw = ->
      drawVisual = requestAnimationFrame(draw)
      analyser.getByteFrequencyData(dataArray)
      ctx.clearRect(0, 0, width, height)

      barWidth = (width / bufferLength) * 2.5
      x = 0

      for i in [0..bufferLength]
        barHeight = dataArray[i]*1.8
        ctx.fillStyle = "rgba(255, 255, 255, .15)"
        ctx.fillRect(x, height - barHeight/2, barWidth, barHeight/2)
        x += barWidth + 1

    draw()

  playback: ->
    txt = @ui.playback.find('p')

    if @daw.playing
      @daw.stop_play()
      @daw.playing = false
      
      @ui.playback.removeClass('playing')
      txt.text('Play Back Your Song')
    else
      @daw.play()
      @daw.playing = true
      @ui.playback.addClass('playing')
      txt.text('Stop Playing')

      setTimeout =>
        @daw.stop_play()
        @daw.playing = false
        @ui.playback.removeClass('playing')
        txt.text('Play Back Your Song')
      , 30000

  next: ->
    if @daw.playing then @daw.stop_play()
    recording = new Recording(audio: @daw)
    recording.save
      success: ->
        App.request('router').navigate('finish', trigger: true)
      error: (a, b, c) ->
        console.error 'something real whack happened'
        console.error a, b, c 

  countdown: (el, sec, cb = (->)) ->
    el.text(sec)
    timer = setInterval =>
      --sec
      if not sec
        clearInterval(timer)
        return cb()
      el.text(sec)
    , 1000

  display_lyrics: ->
    text = @ui.lyrics.find('.text')
    text.text('')

    karaoke
      el: text[0]
      lyrics:
        '0.4': 'I need a dollar, dollar'
        '2.5': 'A dollar that\'s what I need'
        '4.5': 'Hey hey'
        '5.5': 'I need a dollar, dollar'
        '7.5': 'A dollar that\s what I need'
        '9.5': 'Hey hey'
        '10.5': 'I need a dollar, dollar'
        '12.5': 'A dollar that\s what I need'
        '14.5': 'And if I share with you my story'
        '17.0': 'Will you share your dollar with me'
        '19.7': 'pause'
        '20.5': 'Bad times are coming'
        '22.3': 'And I reap what I done sowed'
        '24.7': 'pause'
        '25.5': 'Well let me tell you something'
        '27.8': 'All that glitters ain\'t gold'

module.exports = Record
