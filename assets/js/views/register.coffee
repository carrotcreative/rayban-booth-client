Marionette = require 'backbone.marionette'
Syphon     = require 'backbone.syphon'
App        = require '../application.coffee'
$          = require 'jquery'
require('fancybox')($)


class Register extends Marionette.ItemView
  template: templates.register
  id: 'register'

  ui:
    keyboard: '#keyboard ul'
    input: '.email-box input[type=text]'
    letters: '#keyboard .letter'
    error: '.error'
    rules: '.rules'
    terms: '.terms-and-conditions'
    links: 'a.fancybox'

  events:
    'submit': 'submit'
    'focus @ui.input': 'show_keyboard'
    'click #keyboard .done': 'hide_keyboard'
    'click #keyboard li': 'type'
    'click @ui.rules': 'show_rules'

  initialize: ->
    @shift = false
    @capslock = false
    @router = App.request('router')

  submit: (e) ->
    e.preventDefault()
    form = Syphon.serialize(@)

    if not form.terms
      @ui.error.text('You must agree to the rules')
      @ui.error.show()
      return false

    if not form.email.match(/.*@.*\..*/)
      @ui.error.text('Please enter a valid email')
      @ui.error.show()
      return false

    @model.save(email: form.email)

    @router.navigate('preview', trigger: true)

  show_rules: ->
    @router.navigate('rules', trigger: true)

  show_terms: ->
    @router.navigate('terms', trigger: true)

  show_keyboard: ->
    $('#register').addClass('keyboard-active')
    @ui.keyboard.addClass('active')

  hide_keyboard: ->
    $('#register').removeClass('keyboard-active')
    @ui.keyboard.removeClass('active')

  type: (e) ->
    target = $(e.currentTarget)
    character = target.html()
      
    # Shift key
    if target.hasClass('left-shift')
      @ui.letters.toggleClass('uppercase')
      $('.symbol span').toggle()
      
      @shift = if @shift is true then false else true
      @capslock = false
      return false
    
    # Caps lock
    if target.hasClass('capslock')
      @ui.letters.toggleClass('uppercase')
      @capslock = true
      return false
    
    # Delete
    if target.hasClass('delete')
      html = @ui.input.val()
      
      @ui.input.val(html.substr(0, html.length - 1))
      return false
    
    # Special characters
    if target.hasClass('symbol') then character = $('span:visible', target).html()
    if target.hasClass('space') then character = ' '
    if target.hasClass('tab') then character = "@"
    if target.hasClass('return') then character = ".com"
    if target.hasClass('done') then character = ""
    
    # Uppercase letter
    if target.hasClass('uppercase') then character = character.toUpperCase()
    
    # Remove shift once a key is clicked.
    if @shift is true
      $('.symbol span').toggle()
      if @capslock is false then @ui.letters.toggleClass('uppercase')
      @shift = false

    # Add the character
    @ui.input.val("#{@ui.input.val()}#{character}")

  onShow: ->
    @init_link_modals()

  init_link_modals: ->
    @ui.links.fancybox
      openEffect: 'elastic'
      closeEffect: 'elastic'
      iframe: { preload: false }

module.exports = Register
