Marionette = require 'backbone.marionette'
App        = require '../application.coffee'

class Single extends Marionette.ItemView
  template: (locals) => templates[@options.type](locals)
  id: 'single'

  events:
    'click .back': 'back'

  back: ->
    App.request('router').navigate('', trigger: true)

module.exports = Single
