# Rayban Booth

An interactive karaoke experience :sparkles:

### Setup

- Clone down [rayban-booth-server](https://github.com/carrot/rayban-booth-server)
- Follow the setup instructions and get it running
- Make sure [roots](http://roots.cx) is installed
- Clone this project down and run `npm i`
- Run `roots watch`

### Starting up the client in the booth

- follow instructions to start up the [server](https://github.com/carrot/rayban-booth-server) which will also start up the web server to serve the client
- open Chrome with a flag to always allow access to the microphone by running `/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --use-fake-ui-for-media-stream`
- direct the browser to `http://localhost:1111`
- Enter presentation mode
- get $$$

### Deployment

This app is intended to run locally and not be online at all, therefore we do not have it staged anywhere.
